import { randint, choice } from "../../classes/random";
import { Command } from "../command";

const positiveAdjectives = [
  "accomplished",
  "adaptable",
  "admirable",
  "adorable",
  "adventurous",
  "affable",
  "agreeable",
  "alluring",
  "amiable",
  "amusing",
  "beautiful",
  "brave",
  "bright",
  "charming",
  "cheerful",
  "commendable",
  "congenial",
  "considerate",
  "convivial",
  "cordial",
  "courageous",
  "diligent",
  "diplomatic",
  "distinguished",
  "elegant",
  "eminent",
  "enchanting",
  "engaging",
  "enthusiastic",
  "estimable",
  "fun",
  "genial",
  "graceful",
  "gracious",
  "gregarious",
  "handsome",
  "honorable",
  "inspiring",
  "intuitive",
  "jolly",
  "jovial",
  "kind",
  "kindly",
  "laudable",
  "likable",
  "masterful",
  "pleasant",
  "praiseworthy",
  "refined",
  "refreshing",
  "reliable",
  "remarkable",
  "reputable",
  "skillful",
  "smart",
  "solid",
  "strong",
  "sweet",
  "sympathetic",
  "thoughtful",
  "wonderful",
  "worthy",
  "amazing",
];

const sentences = [
  "actions speak louder than words, and [yours] tell an incredible story",
  "all of [your] practice really shows in [your] work",
  "any team would be lucky to have [you2] on it",
  "aside from food [you're] my favorite",
  "babies and small animals probably love [you]",
  "being around [you2] is like being on a happy little vacation",
  "being around [you2] makes everything better",
  "colors seem brighter when [you're] around",
  "everyone gets knocked down sometimes, but [you] always get back up and keep going",
  "everything would be better if more people were like [you2]",
  "how do [you] keep being so funny and making everyone laugh?",
  "i always learn so many interesting and thought-provoking things from [you2]",
  "i am proud of how far [you] have come in [your] journey",
  "i appreciate [you2]",
  "i bet [you] make babies smile",
  "i bet [you] sweat glitter",
  "i can see so much warmth in [your] eyes",
  "i can’t get over how wise [you] are — [your] wisdom in everything amazes me",
  "i haven’t met a person who’s as nice and caring as [you2]",
  "i like [your] style",
  "i think [you] are perfect just the way [you] are",
  "i truly respect and value [your] thoughts and ideas",
  "if cartoon bluebirds were real, a bunch of them would be sitting on [your] shoulders singing right now",
  "if [you] were a scented candle they’d call it perfectly imperfect (and it would smell like summer)",
  "in high school i bet [you] were voted “most likely to keep being awesome!”",
  "is that [your] picture next to “charming” in the dictionary?",
  "jokes are funnier when [you] tell them",
  "nobody makes me happier than [target]",
  "on a scale from 1 to 10, [you're] an 11",
  "other people, including myself, aspire to be like [you2] one day",
  "our community is better because [you're] in it",
  "somehow [you] make time stop and fly at the same time",
  "someone is getting through something hard right now because [you've] got their back",
  "that thing [you] don’t like about [yourself] is what makes [you2] so interesting",
  "the people [you] love are lucky to have [you2] in their lives",
  "the way [you] treasure [your] loved ones is incredible",
  "there’s ordinary, and then there’s [target]",
  "when [you] make up [your] mind about something, nothing stands in [your] way",
  "when [you] say, “i meant to do that,” i totally believe [you2]",
  "when [you're] not afraid to be [yourself] is when [you're] most incredible",
  "who raised [you2]? they deserve a medal for a job well done",
  "[you] always know how to find that silver lining",
  "[you] always know just what to say",
  "[you] are a muse for me and so many others",
  "[you] are making a difference",
  "[you] are one of a kind and it continues to impress me",
  "[you] are so smart",
  "[you] are such a joy to be around",
  "[you] are the most incredible when [you're] not afraid to be [yourself]",
  "[you] are the most perfect [you2] there is",
  "[you] bring out the best in other people",
  "[you] can accomplish anything that [you] put [your] mind to",
  "[you] deserve a hug right now",
  "[you] have a good head on [your] shoulders",
  "[you] have a great sense of humor",
  "[you] have such a positive charisma",
  "[you] have such good taste",
  "[you] have the best ideas",
  "[you] have the best laugh",
  "[you] have the courage of [your] convictions",
  "[you] light up the room",
  "[you] look great today",
  "[you] seem to really know who [you] are",
  "[you] should be proud of [yourself]",
  "[you] should be thanked more often! so thank [target]",
  "[you're] accomplishing so much! every day [you] evolve into a better person than who [you] were the day before",
  "[you're] more amazing than [you] will ever realize",
  "[your] creative potential seems limitless",
  "[your] dedication to [your] craft is inspiring",
  "[your] intelligence and curiosity inspire me",
  "[your] kindness is a balm to all who encounter it",
  "[your] perspective is refreshing",
  "[your] thoughts and ideas are so creative, which has allowed [you2] to refine [your] talent into something unique from others",
  "[your] voice is magnificent",
  "[you're] a candle in the darkness",
  "[you're] a gift to those around [you2]",
  "[you're] a great example to others",
  "[you're] a great listener",
  "[you're] a smart cookie",
  "[you're] all that and a super-size bag of chips",
  "[you're] always learning new things and trying to better [yourself], which is awesome",
  "[you're] an awesome friend",
  "[you're] better than a triple-scoop ice cream cone with sprinkles",
  "[you're] even better than a unicorn, because [you're] real",
  "[you're] even more beautiful on the inside than [you] are on the outside",
  "[you're] gorgeous — and that’s the least interesting thing about [you2], too",
  "[you're] great at figuring stuff out",
  "[you're] like a breath of fresh air",
  "[you're] like sunshine on a rainy day",
  "[you're] more helpful than [you] realize",
  "[you're] really something special",
  "[you're] someone’s reason to smile",
  "[you're] nicer than a day on the beach",
  "[you] make me think of beautiful things, like strawberries",
  "[your] every thought and motion contributes to the beauty of the universe",
];

export const commandCompliment = new Command(
  ["COMPLIMENT"],
  ["compliment", "compliment me", "compliment <username>"],
  (message, args) => {
    const isMe = args.length === 0 || args[0].toLowerCase() === "me";
    if (randint(0, 1) === 0) {
      message.channel.send(
        `${!isMe ? `${args[0]} is` : "you are"} ${choice(
          positiveAdjectives
        )}!!!`
      );
    } else {
      let sentence = choice(sentences);
      if (isMe) {
        sentence = sentence
          .replace(/\[target\]/g, "you")
          .replace(/\[you2\]/g, "you")
          .replace(/\[/g, "")
          .replace(/\]/g, "");
      } else {
        sentence = sentence
          .replace(/\[target\]/g, `${args[0]}`)
          .replace(/\[yourself\]/g, "themselves")
          .replace(/\[yours\]/g, "theirs")
          .replace(/\[your\]/g, "their")
          .replace(/\[you're\]/g, "they're")
          .replace(/\[you\]/g, "they")
          .replace(/\[you2\]/g, "them")
          .replace(/\[you've\]/g, "they've");
      }
      sentence += "!!!";
      // sentence = sentence.charAt(0).toUpperCase() + sentence.slice(1)
      message.channel.send(sentence);
    }
  }
);
