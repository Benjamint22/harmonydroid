import { load } from "cheerio";
import "isomorphic-fetch";

export interface SubredditRSSEntry {
  id: string;
  title: string;
}

export class RedditPost {
  constructor(public readonly id: string, public readonly title: string) {}
}

export class RedditRequester {
  /**
   * Fetches a TIL from Reddit.
   */
  static async fetchTIL(): Promise<RedditPost[]> {
    const response = await fetch("https://www.reddit.com/r/todayilearned/.rss");

    const xml = load(await response.text(), { xmlMode: true });

    return xml("feed > entry")
      .toArray()
      .map(
        (entry) =>
          new RedditPost(xml("id", entry).text(), xml("title", entry).text())
      );
  }
}
