import { Command } from "../command";
import {
  GenerateNameParameters,
  RandomNameGenerator,
} from "../../classes/random_name_generator";
import { choice } from "../../classes/random";

const doc = [
  "generate name",
  "generate name <first_characters>",
  "generate name <length>",
  "generate name <first_characters> <length>",
];

export const commandGenerate = new Command(
  ["GENERATE", "GEN"],
  doc,
  async (message, args) => {
    if (args.length > 0) {
      if (args[0].toLowerCase() === "name") {
        const params: GenerateNameParameters = {};
        if (args.length > 1) {
          if (args.length > 2) {
            params.firstCharacters = args[1];
            params.length = Number.parseInt(args[2], 10);
            if (Number.isNaN(params.length)) {
              message.channel.send(
                `Sorry! I cannot recognize "${args[2]}" as a length.`
              );
              return;
            }
          } else {
            const length = Number.parseInt(args[1], 10);
            if (Number.isNaN(length)) {
              params.firstCharacters = args[1];
            } else {
              params.length = length;
            }
          }
        }
        let name: string;
        try {
          name = await RandomNameGenerator.generateName(params);
        } catch (error) {
          message.channel.send(
            "Sorry! An error occured when trying to load the name generation model."
          );
          console.error(error);
          return;
        }
        message.channel.send(
          choice([
            "Do you like [name]?",
            "I tried my best and generated [name]!",
            "Hopefully [name] is what you wanted.",
            "Here comes [name]!",
            "What about [name]?",
          ]).replace(/\[name\]/g, name)
        );
      } else {
        message.channel.send(
          `Unfortunately I cannot generate this. Here are the instructions:\`\`\`${doc.join(
            "\n"
          )}\`\`\``
        );
      }
    } else {
      message.channel.send(
        `Invalid parameters. Here are the instructions:\`\`\`${doc.join(
          "\n"
        )}\`\`\``
      );
    }
  }
);
