import { Command } from "../../command";
import { RedditRequester } from "../../../classes/reddit";
import { choice } from "../../../classes/random";

let checkedIds: string[] = [];

export const commandTIL = new Command(
  ["TIL", "TODAYILEARNED"],
  ["til"],
  async (message) => {
    const allPosts = await RedditRequester.fetchTIL();
    let availablePosts = allPosts.filter(
      (post) => !checkedIds.includes(post.id)
    );
    if (availablePosts.length === 0) {
      checkedIds = [];
      availablePosts = allPosts;
    }
    const chosenPost = choice(availablePosts);
    checkedIds.push(chosenPost.id);
    await message.channel.send(chosenPost.title);
  }
);
