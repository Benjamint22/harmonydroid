import { readFileSync } from "fs";

function loadFileSync(path: string) {
  try {
    return readFileSync(path, { encoding: "utf8" }).toString();
  } catch (e) {
    return null;
  }
}

function loadEnvFile<T extends Record<string, string>>(env = "") {
  const content = loadFileSync(`./.env${env ? `.${env}` : ""}`);
  if (content) {
    const result = {} as T;
    content
      .toString()
      .replace("\r", "")
      .split("\n")
      .forEach((pair) => {
        const [key, value] = pair.split("=");
        if (key) {
          result[key.trim() as keyof T] = (
            value ? value.trim() : ""
          ) as T[keyof T];
        }
      });
    return result;
  }
  return {};
}

export interface Environment {
  HARMONYDROID_TOKEN: string;
}

export const ActiveEnvironment = {
  ...loadEnvFile(),
  ...loadEnvFile("test"),
} as Environment;
