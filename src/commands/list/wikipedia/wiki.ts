import { MessageEmbed } from "discord.js";
import { convert } from "html-to-text";
import { Command } from "../../command";
import { fetchRandomWikipediaArticle } from "../../../classes/wikipedia";

export const commandWiki = new Command(
  ["WIKI", "WIKIPEDIA"],
  ["wiki"],
  async (message) => {
    await message.channel.sendTyping();
    const article = await fetchRandomWikipediaArticle();
    if (!article) {
      await message.channel.send("Oops! I couldn't find an article.");
      return;
    }
    const body = convert(article.shortExtract, {
      unorderedListItemPrefix: " • ",
    });
    const embed = new MessageEmbed({
      title: article.title,
      url: article.url,
      color: "#e8e991",
    });
    if (article.imageUrl) {
      embed.setImage(article.imageUrl);
    } else if (article.thumbnailUrl) {
      embed.setThumbnail(article.thumbnailUrl);
    }
    await message.channel.send({
      content: body,
      embeds: [embed],
    });
  }
);
