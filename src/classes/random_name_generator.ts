import { readFileAsync } from "../utils/file-utils";
import { randomFromLikeliness } from "./random";

export interface NameGeneratorModel {
  /**
   * Amount of names parsed.
   */
  namesParsed: string;
  /**
   * Amount of names with a given length.
   */
  namesWithLength: Record<number, number>;
  /**
   * Amount of names starting with a given letter.
   */
  namesStartingWith: Record<string, number>;
  /**
   * Amount of times a letter is followed by another.
   */
  lettersFollowing: Record<string, Record<string, number>>;
}

export interface GenerateNameParameters {
  /**
   * The first characters in the name.
   */
  firstCharacters?: string;
  /**
   * The length of the name between 2 and 99.
   */
  length?: number;
}

export class RandomNameGenerator {
  private static model: NameGeneratorModel;

  private static async getModel(): Promise<NameGeneratorModel> {
    if (!RandomNameGenerator.model) {
      let modelJSONBuffer: Buffer;
      try {
        modelJSONBuffer = await readFileAsync("./src/files/model.json");
      } catch (error) {
        throw error;
      }
      return (RandomNameGenerator.model = JSON.parse(
        modelJSONBuffer.toString()
      ));
    }
    return RandomNameGenerator.model;
  }

  /**
   * Generates a random name.
   * @param params The parameters to generate a name with.
   * @returns The generated name.
   * @throws {NodeJS.ErrnoException} If the model could not be loaded.
   */
  static async generateName(params: GenerateNameParameters): Promise<string> {
    const model = await RandomNameGenerator.getModel();
    const nameLength = params.length
      ? Math.min(Math.max(params.length, 2), 99)
      : randomFromLikeliness(model.namesWithLength);
    let name =
      params.firstCharacters || randomFromLikeliness(model.namesStartingWith);
    name = name.charAt(0).toUpperCase() + name.substr(1).toLowerCase();
    let letter = name.charAt(name.length - 1).toLowerCase();
    for (let i = name.length; i < nameLength; i += 1) {
      letter = randomFromLikeliness(model.lettersFollowing[letter]);
      name += letter;
    }
    return name;
  }
}
