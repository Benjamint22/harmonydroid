// Imports
import {
  Client,
  Intents,
  PermissionResolvable,
  Permissions,
  TextBasedChannels,
} from "discord.js";
import { ActiveEnvironment } from "./classes/env";
import { Command } from "./commands/command";
import { commandCompliment } from "./commands/list/compliment";
import { commandGenerate } from "./commands/list/generate";
import { commandHelp } from "./commands/list/help";
import { commandTIL } from "./commands/list/reddit/til";
import { commandWiki } from "./commands/list/wikipedia/wiki";

// Commands
const commands = [
  commandHelp,
  commandCompliment,
  commandGenerate,
  commandTIL,
  commandWiki,
];

// Constants
const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.DIRECT_MESSAGES,
    Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
  ],
});

// Typedefs
const PFLAGS = Permissions.FLAGS;

// Globals
const commandPrefixes: string[] = [];

/**
 * Checks if the bot has the given permissions in the given channel.
 * @param channel The channel to check.
 * @param needed The permissions to check.
 * @returns Whether the bot has those permissions.
 */
function hasPermissions(
  channel: TextBasedChannels,
  needed: PermissionResolvable
) {
  if (channel.type === "DM") {
    return true;
  }
  const permissions = channel.permissionsFor(client.user!);
  return !!permissions?.has(needed);
}

client.on("messageCreate", (message) => {
  (async () => {
    if (
      !hasPermissions(message.channel, [
        PFLAGS.SEND_MESSAGES,
        PFLAGS.ATTACH_FILES,
      ])
    ) {
      return;
    }
    if (message.author === client.user) {
      return;
    }
    let content: string | null = null;
    for (const prefix of commandPrefixes) {
      if (message.content.startsWith(prefix)) {
        content = message.content.substring(prefix.length);
        break;
      }
    }
    if (content !== null) {
      const { cmd, args } = Command.parse(content);
      const command = Command.getFrom(cmd);
      if (command !== null) {
        command.body(message, args);
      }
    } else if (message.content.toLowerCase().indexOf("owo") !== -1) {
      message.channel.send("owo");
    }
  })().catch((err) => {
    console.error(err);
  });
});

client.on("ready", () => {
  Command.instantiateCommands(commands);
  commandPrefixes.push(".");
  commandPrefixes.push(`${client.user} `);
  console.log("Logged in as");
  console.log(client.user!.tag);
  console.log(client.user!.id);
  console.log("------");
});

function handleShutdown() {
  let active = true;
  [
    "SIGINT",
    "SIGTERM",
    "SIGQUIT",
    "SIGUSR1",
    "SIGUSR2",
    "uncaughtException",
    "disconnect",
  ].forEach((exitEvent) => {
    process.on(exitEvent, () => {
      console.warn(`Received ${exitEvent}.`);
      if (active) {
        console.log("Attempting to gracefully disconnect from Discord");
        active = false;
        client.destroy();
        console.log("Disconnected cleanly from Discord");
        process.exit(0);
      }
    });
  });
}

async function login() {
  const token =
    process.env.HARMONYDROID_TOKEN || ActiveEnvironment.HARMONYDROID_TOKEN;
  if (!token) {
    console.error(
      "No Discord API token was found. Try setting the DISCORDDROID_TOKEN variable."
    );
    return;
  }
  await client.login(token);
  handleShutdown();
}

login();
