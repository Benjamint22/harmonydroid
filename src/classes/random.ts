import { keysOf, valuesOf } from "../utils/object-utils";

/**
 * Fetches a random key from a set where the values are their chances of being picked.
 * @returns The picked item.
 */
export function randomFromLikeliness<T extends string>(
  dataset: Record<T, number>
) {
  let totalChances = 0;
  const chances = valuesOf(dataset);
  chances.forEach((val) => {
    totalChances += val;
  });
  const pick = Math.floor(Math.random() * totalChances);
  let pickIndex = 0;
  let cummulatedChances = chances[pickIndex];
  while (cummulatedChances < pick) {
    pickIndex += 1;
    cummulatedChances += chances[pickIndex];
  }
  return keysOf(dataset)[pickIndex];
}

/**
 * Generates a random integer between min and max inclusively.
 * @param min The minimum number to generate (inclusively).
 * @param max The maximum number to generate (inclusively).
 */
export function randint(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Picks a random element from an Array.
 * @param arr The array to pick from.
 * @returns The element picked from the array.
 */
export function choice<T>(arr: T[]) {
  return arr[randint(0, arr.length - 1)];
}

/**
 * Creates a shuffled copy of an array in a random order.
 * @param arr The array to shuffle
 * @returns A copy of the given array but shuffled.
 */
export function shuffleArray<T>(arr: T[]) {
  const slicedArray = arr.slice(0);
  for (let i = slicedArray.length - 1; i >= 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    [slicedArray[i], slicedArray[j]] = [slicedArray[j], slicedArray[i]];
  }
  return slicedArray;
}
