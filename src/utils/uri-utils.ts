import { entriesOf } from "./object-utils";

export function addParamsToUrl(
  url: string,
  params: Record<string, string | number | boolean>
) {
  return (
    url +
    "?" +
    entriesOf(params)
      .map(([key, value]) => `${key}=${value}`)
      .join("&")
  );
}
