import { Command } from "../command";

export const commandHelp = new Command(["HELP"], ["help"], (message) => {
  const descriptions: string[] = [];
  for (const command of Command.getCommands() ?? []) {
    descriptions.push(...command.help);
  }
  message.channel.send(
    `\`\`\`
Prefix: .

Current Commands:
  ${descriptions.join("\n  ")}
\`\`\``
  );
});
