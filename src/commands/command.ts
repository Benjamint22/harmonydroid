import { Message } from "discord.js";

/**
 * The body of a command.
 * @param message The source message.
 * @param args The parameters.
 */
export type CommandBody = (
  message: Message,
  args: string[]
) => Promise<any> | void;

let staticCommands: Command[] | null = null;

export class Command {
  /**
   * Instantiates a command.
   * @param aliases The aliases supported for this command.
   * @param help Help information for this command.
   * @param body The function executed by the command.
   */
  constructor(
    public readonly aliases: string[],
    public readonly help: string[],
    public readonly body: CommandBody
  ) {}

  /**
   * Retrieves a command from a string.
   */
  static getFrom(cmd: string) {
    for (const command of staticCommands ?? []) {
      for (const alias of command.aliases) {
        if (alias.toLowerCase() === cmd.toLowerCase()) {
          return command;
        }
      }
    }
    return null;
  }

  /**
   * Instantiates every command.
   */
  static instantiateCommands(commands: Command[]) {
    staticCommands = commands;
  }

  /**
   * Translates text into two arrays.
   * @param text The text to translate.
   */
  static parse(text: string) {
    const args = text.split(" ");
    const cmd = args[0];
    args.shift();
    return { cmd, args };
  }

  /**
   * Fetches an array of commands.
   */
  static getCommands() {
    return staticCommands;
  }
}

module.exports = { Command };
