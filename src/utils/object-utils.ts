export function entriesOf<K extends string | number, V>(obj: Record<K, V>) {
  return Object.entries(obj) as [K, V][];
}

export function keysOf<K extends string | number>(obj: Record<K, any>) {
  return Object.keys(obj) as K[];
}

export function valuesOf<V>(obj: Record<string | number, V>) {
  return Object.values(obj) as V[];
}
