import { valuesOf } from "../utils/object-utils";
import "isomorphic-fetch";
import { addParamsToUrl } from "../utils/uri-utils";

/**
 * Represents a Wikipedia article.
 */
export class WikipediaArticle {
  constructor(
    public readonly id: number,
    public readonly title: string,
    public readonly shortExtract: string,
    public readonly url: string,
    public readonly thumbnailUrl?: string,
    public readonly imageUrl?: string
  ) {}
}

interface WikipediaArticleWithoutThumbnailBody {
  pageid: number;
  ns: number;
  title: string;
  extract: string;
}

export interface WikipediaArticleWithThumbnailbody
  extends WikipediaArticleWithoutThumbnailBody {
  thumbnail: {
    source: string;
  };
  pageimage: string;
}

type WikipediaArticleBody =
  | WikipediaArticleWithoutThumbnailBody
  | WikipediaArticleWithThumbnailbody;

function isArticleWithThumbnail(
  articleBody: WikipediaArticleBody
): articleBody is WikipediaArticleWithThumbnailbody {
  return "thumbnail" in articleBody && "pageimage" in articleBody;
}

async function fetchRandomArticleTitle(tries = 3): Promise<string | null> {
  if (tries <= 0) {
    return null;
  }
  const response = await fetch("https://en.wikipedia.org/wiki/Special:Random");
  return (
    /wiki\/(.*)$/g.exec(response.url)?.[1] ?? fetchRandomArticleTitle(tries - 1)
  );
}

function getImageFromThumbnail(imageName: string, thumbnailUrl: string) {
  const indexOfImageName = thumbnailUrl.indexOf(imageName);
  if (indexOfImageName === -1) {
    return null;
  }
  return thumbnailUrl
    .substr(0, indexOfImageName + imageName.length)
    .replace("thumb/", "");
}

async function fetchWikipediaArticle(titleToFetch: string) {
  const response = await fetch(
    addParamsToUrl("https://en.wikipedia.org/w/api.php", {
      action: "query",
      format: "json",
      prop: "extracts|pageimages",
      exintro: true,
      exsentences: 2,
      titles: titleToFetch,
    })
  );
  const chosenPageBody = valuesOf(
    (await response.json()).query.pages
  )[0] as WikipediaArticleBody;
  if (!chosenPageBody) {
    return null;
  }
  const { pageid, title, extract } = chosenPageBody;
  const url = `https://en.wikipedia.org/wiki/${titleToFetch}`;
  if (!isArticleWithThumbnail(chosenPageBody)) {
    return new WikipediaArticle(pageid, title, extract, url);
  }
  const thumbnailUrl = chosenPageBody.thumbnail.source;
  const imageUrl = getImageFromThumbnail(
    chosenPageBody.pageimage,
    thumbnailUrl
  );
  if (!imageUrl) {
    return new WikipediaArticle(pageid, title, extract, url, thumbnailUrl);
  }
  return new WikipediaArticle(
    pageid,
    title,
    extract,
    url,
    thumbnailUrl,
    imageUrl
  );
}

/**
 * Fetches a random article from en.wikipedia.org
 */
export async function fetchRandomWikipediaArticle(): Promise<WikipediaArticle | null> {
  const title = await fetchRandomArticleTitle();
  if (!title) {
    return null;
  }
  return fetchWikipediaArticle(title);
}
