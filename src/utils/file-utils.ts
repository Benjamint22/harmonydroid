import { readFile } from "fs";

/**
 * Asynchronously reads the entire contents of a file.
 * @param path
 * A path to a file. If a URL is provided, it must use the file: protocol.
 * If a file descriptor is provided, the underlying file will not be closed automatically.
 * @param options
 * An object that may contain an optional flag. If a flag is not provided, it defaults to 'r'.
 * @throws {NodeJS.ErrnoException}
 */
export function readFileAsync(path: string): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    readFile(path, {}, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
